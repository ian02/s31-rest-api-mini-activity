const express = require ('express');
const router = express.Router ();

const productController = require ('../controllers/productController');

//Route for getting all products
router.get('/', (req, res) => {
	productController.getAllProducts().then (resultFromController => res.send (resultFromController));
});


//Route to add new product
router.post ('/', (req, res) => {
	productController.createProduct (req.body).then(resultFromController => res.send (resultFromController))
});


//Route for getting a specific id/record
router.get ('/:id', (req, res) => {
	productController.getProduct (req.params.id).then (resultFromController => res.send (resultFromController))
});


//Route to delete a product
router.delete ('/delete/:id', (req, res) => {
	productController.deleteProduct (req.params.id).then (resultFromController => res.send (resultFromController))
});

module.exports = router;