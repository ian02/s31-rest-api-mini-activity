const Product = require ('../models/product');


//Get all products
module.exports.getAllProducts = () => {
	return Product.find ({}).then (result => {
		return result;
	})
} 

//Create a new product and check for duplicates
module.exports.createProduct = (requestBody) => {
	return Product.findOne ({name: requestBody.name}).then ((result, error) => {
		if (result !== null && result.name == requestBody.name) {
			return ('Duplicate product found!')
		} else {
			let newProduct = new Product ({
				name: requestBody.name,
				price: requestBody.price				
			})
			return newProduct.save().then ((result, error) => {
				if (error){
					console.log (error);
					return false;
				} else {
					return ('New product created.');
				}
			})
		}
	})
}

//Get single product
module.exports.getProduct = (productId) => {
	return Product.findById (productId).then ((result, error) => {
		if (error) {
			console.log (error)
			return false;
		} else {
			return result;
		}
	})
}

//Delete product
module.exports.deleteProduct = (productId) => {
	return Product.findByIdAndRemove (productId).then ((removeProduct, error) => {
		if (error){
			console.log (error)
			return false;
		} else {
			return removeProduct
		}
	})
}